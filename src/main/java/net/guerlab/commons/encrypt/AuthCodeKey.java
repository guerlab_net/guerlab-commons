/*
 * Copyright 2018-2024 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.commons.encrypt;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * 混淆加密秘钥对象
 *
 * @author guer
 */
final class AuthCodeKey {

    /**
     * 秘钥A
     */
    private final String keyA;

    /**
     * 秘钥B
     */
    private final String keyB;

    /**
     * 通过原始秘钥和指定字符编码集设置秘钥A和秘钥B
     *
     * @param originalSecretKey
     *         原始秘钥
     * @throws NullPointerException
     *         当originalSecretKey为空的时候抛出NullPointerException异常
     */
    AuthCodeKey(String originalSecretKey) {
        if (originalSecretKey == null) {
            throw new NullPointerException("original secret key can not to be null");
        }
        String key = DigestUtils.md5Hex(originalSecretKey);
        keyA = DigestUtils.md5Hex(key.substring(0, 16));
        keyB = DigestUtils.md5Hex(key.substring(16, 32));
    }

    /**
     * 获取秘钥A
     *
     * @return 秘钥A
     */
    String getKeyA() {
        return keyA;
    }

    /**
     * 获取秘钥B
     *
     * @return 秘钥B
     */
    String getKeyB() {
        return keyB;
    }
}
