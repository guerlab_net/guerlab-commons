/*
 * Copyright 2018-2024 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.commons.encrypt;

import java.nio.charset.Charset;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * RC4算法助手
 *
 * @author guer
 */
public final class Rc4Helper {

    private static final Logger LOGGER = LoggerFactory.getLogger(Rc4Helper.class);

    private Rc4Helper() {
    }

    /**
     * 使用默认编码进行编码
     *
     * @param input
     *         待编码内容
     * @param pass
     *         混淆编码字符串
     * @return 编码后内容
     */
    public static byte[] encode(byte[] input, String pass) {
        return encode(input, pass, Charset.defaultCharset().name());
    }

    /**
     * 使用指定编码进行编码
     *
     * @param input
     *         待编码内容
     * @param pass
     *         混淆编码字符串
     * @param charsetName
     *         混淆编码字符串编码格式
     * @return 编码后内容
     * @throws NullPointerException
     *         charsetName为空时抛出NullPointerException异常
     */
    public static byte[] encode(byte[] input, String pass, String charsetName) {
        if (charsetName == null) {
            throw new NullPointerException("charsetName can not be null");
        }
        if (input == null || pass == null) {
            return new byte[0];
        }
        try {
            byte[] output = new byte[input.length];
            byte[] mBox = getKey(pass.getBytes(charsetName));

            int i = 0;
            int j = 0;

            for (int offset = 0; offset < input.length; offset++) {
                i = (i + 1) % mBox.length;
                j = (j + (mBox[i] + 256) % 256) % mBox.length;

                byte temp = mBox[i];
                mBox[i] = mBox[j];
                mBox[j] = temp;
                byte a = input[offset];

                byte b = mBox[(toInt(mBox[i]) + toInt(mBox[j])) % mBox.length];

                output[offset] = (byte) ((long) a ^ (long) toInt(b));
            }

            return output;
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
            return new byte[0];
        }
    }

    private static byte[] getKey(byte[] pass) {
        int kLen = 256;
        byte[] mBox = new byte[kLen];

        for (int i = 0; i < kLen; i++) {
            mBox[i] = (byte) i;
        }

        int j = 0;
        for (int i = 0; i < kLen; i++) {
            j = (j + (mBox[i] + 256) % 256 + pass[i % pass.length]) % kLen;
            byte temp = mBox[i];
            mBox[i] = mBox[j];
            mBox[j] = temp;
        }

        return mBox;
    }

    private static int toInt(byte b) {
        return (b + 256) % 256;
    }
}
