/*
 * Copyright 2018-2024 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.commons.number;

import java.util.Arrays;

/**
 * 数值助手
 *
 * @author guer
 */
public final class NumberHelper {

    private NumberHelper() {
    }

    /**
     * 判断是否相等
     *
     * @param a
     *         数值a
     * @param b
     *         数值b
     * @param <T>
     *         数值类型
     * @return 是否相等
     */
    public static <T extends Comparable<T>> boolean isEquals(T a, T b) {
        return a != null && b != null && a.compareTo(b) == 0;
    }

    /**
     * 判断是否大于0
     *
     * @param number
     *         数值
     * @return 是否大于0
     */
    public static boolean greaterZero(Number number) {
        return number != null && number.doubleValue() > 0;
    }

    /**
     * 判断是否大于等于0
     *
     * @param number
     *         数值
     * @return 是否大于等于0
     */
    public static boolean greaterOrEqualZero(Number number) {
        return number != null && number.doubleValue() >= 0;
    }

    /**
     * 判断是否小于0
     *
     * @param number
     *         数值
     * @return 是否小于0
     */
    public static boolean lessZero(Number number) {
        return number != null && number.doubleValue() < 0;
    }

    /**
     * 判断是否小于等于0
     *
     * @param number
     *         数值
     * @return 是否小于等于0
     */
    public static boolean lessOrEqualZero(Number number) {
        return number != null && number.doubleValue() <= 0;
    }

    /**
     * 判断是否大于0
     *
     * @param numbers
     *         数值列表
     * @return 是否大于0
     */
    public static boolean anyGreaterZero(Number... numbers) {
        if (numbers == null || numbers.length == 0) {
            return false;
        }
        return Arrays.stream(numbers).anyMatch(NumberHelper::greaterZero);
    }

    /**
     * 判断是否大于0
     *
     * @param numbers
     *         数值列表
     * @return 是否大于0
     */
    public static boolean allGreaterZero(Number... numbers) {
        if (numbers == null || numbers.length == 0) {
            return false;
        }
        return Arrays.stream(numbers).allMatch(NumberHelper::greaterZero);
    }

    /**
     * 判断是否大于等于0
     *
     * @param numbers
     *         数值列表
     * @return 是否大于等于0
     */
    public static boolean anyGreaterOrEqualZero(Number... numbers) {
        if (numbers == null || numbers.length == 0) {
            return false;
        }
        return Arrays.stream(numbers).anyMatch(NumberHelper::greaterOrEqualZero);
    }

    /**
     * 判断是否大于等于0
     *
     * @param numbers
     *         数值列表
     * @return 是否大于等于0
     */
    public static boolean allGreaterOrEqualZero(Number... numbers) {
        if (numbers == null || numbers.length == 0) {
            return false;
        }
        return Arrays.stream(numbers).allMatch(NumberHelper::greaterOrEqualZero);
    }

    /**
     * 判断是否小于0
     *
     * @param numbers
     *         数值列表
     * @return 是否小于0
     */
    public static boolean anyLessZero(Number... numbers) {
        if (numbers == null || numbers.length == 0) {
            return false;
        }
        return Arrays.stream(numbers).anyMatch(NumberHelper::lessZero);
    }

    /**
     * 判断是否小于0
     *
     * @param numbers
     *         数值列表
     * @return 是否小于0
     */
    public static boolean allLessZero(Number... numbers) {
        if (numbers == null || numbers.length == 0) {
            return false;
        }
        return Arrays.stream(numbers).allMatch(NumberHelper::lessZero);
    }

    /**
     * 判断是否小于等于0
     *
     * @param numbers
     *         数值列表
     * @return 是否小于等于0
     */
    public static boolean anyLessOrEqualZero(Number... numbers) {
        if (numbers == null || numbers.length == 0) {
            return false;
        }
        return Arrays.stream(numbers).anyMatch(NumberHelper::lessOrEqualZero);
    }

    /**
     * 判断是否小于等于0
     *
     * @param numbers
     *         数值列表
     * @return 是否小于等于0
     */
    public static boolean allLessOrEqualZero(Number... numbers) {
        if (numbers == null || numbers.length == 0) {
            return false;
        }
        return Arrays.stream(numbers).allMatch(NumberHelper::lessOrEqualZero);
    }

}
