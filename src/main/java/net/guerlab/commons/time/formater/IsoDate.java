package net.guerlab.commons.time.formater;

import net.guerlab.commons.time.FormatSupplier;

import java.time.format.DateTimeFormatter;

/**
 * iso日期格式
 *
 * @author guer
 */
public class IsoDate implements FormatSupplier {

    public static final IsoDate INSTANCE = new IsoDate();

    @Override
    public DateTimeFormatter get() {
        return DateTimeFormatter.ISO_DATE;
    }
}
