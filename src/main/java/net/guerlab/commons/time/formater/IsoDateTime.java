package net.guerlab.commons.time.formater;

import net.guerlab.commons.time.FormatSupplier;

import java.time.format.DateTimeFormatter;

/**
 * iso日期时间格式
 *
 * @author guer
 */
public class IsoDateTime implements FormatSupplier {

    @Override
    public DateTimeFormatter get() {
        return DateTimeFormatter.ISO_DATE_TIME;
    }
}
