package net.guerlab.commons.time.formater;

import net.guerlab.commons.time.FormatSupplier;

import java.time.format.DateTimeFormatter;

/**
 * iso时间格式
 *
 * @author guer
 */
public class IsoLocalTime implements FormatSupplier {

    @Override
    public DateTimeFormatter get() {
        return DateTimeFormatter.ISO_LOCAL_TIME;
    }
}
