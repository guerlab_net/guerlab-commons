package net.guerlab.commons.time.formater;

import net.guerlab.commons.time.FormatSupplier;

import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

/**
 * 标准日期格式 "yyyy-MM-dd HH:mm:ss.[S{0, 9}]"
 *
 * @author guer
 */
public class StandardDate implements FormatSupplier {

    public static final StandardDate INSTANCE = new StandardDate();

    @Override
    public DateTimeFormatter get() {
        DateTimeFormatter isoLocalDateTime = new DateTimeFormatterBuilder().parseCaseInsensitive()
                .append(DateTimeFormatter.ISO_LOCAL_DATE).appendLiteral(' ').append(DateTimeFormatter.ISO_LOCAL_TIME)
                .toFormatter();

        return new DateTimeFormatterBuilder().append(isoLocalDateTime).optionalStart().appendOffsetId().optionalStart()
                .appendLiteral('[').parseCaseSensitive().appendZoneRegionId().appendLiteral(']').toFormatter();
    }
}
