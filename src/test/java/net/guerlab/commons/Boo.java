package net.guerlab.commons;

/**
 * @author guer
 */
public class Boo {

    public String publicValue;

    private String privateValue;

    public String getPrivateValue() {
        return privateValue;
    }

    public void setPrivateValue(String privateValue) {
        this.privateValue = privateValue;
    }
}
