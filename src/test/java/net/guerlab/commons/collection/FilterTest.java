package net.guerlab.commons.collection;

import net.guerlab.commons.ValueWrapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/**
 * @author guer
 */
public class FilterTest {

    private Collection<ValueWrapper> wrappers;

    @Before
    public void before() {
        wrappers = Arrays.asList(new ValueWrapper(1), new ValueWrapper(2), null);
    }

    @Test
    public void test1() {
        Assert.assertEquals(1,
                CollectionUtil.filters(wrappers, Collections.singletonList(wrapper -> wrapper.value % 2 == 0)).size());
    }

    @Test
    public void empty() {
        Assert.assertEquals(0, CollectionUtil
                .filters((Collection<ValueWrapper>) null, Collections.singletonList(wrapper -> wrapper.value % 2 == 0))
                .size());
    }
}
