package net.guerlab.commons.collection;

import net.guerlab.commons.ValueWrapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;

/**
 * @author guer
 */
public class ForEachTest {

    private Collection<ValueWrapper> wrappers;

    @Before
    public void before() {
        wrappers = Arrays.asList(new ValueWrapper(1), new ValueWrapper(2), null);
    }

    @Test
    public void test1() {
        CollectionUtil.forEach(wrappers, wrapper -> wrapper.value = wrapper.value * 2);
        Assert.assertEquals(wrappers, Arrays.asList(new ValueWrapper(2), new ValueWrapper(4), null));
    }

    @Test(expected = NullPointerException.class)
    public void test2() {
        CollectionUtil.forEach((Collection<ValueWrapper>) null, wrapper -> wrapper.value = wrapper.value * 2);
    }
}
