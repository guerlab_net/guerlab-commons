package net.guerlab.commons.collection;

import net.guerlab.commons.ValueWrapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

/**
 * @author guer
 */
public class GroupTest {

    private Collection<ValueWrapper> wrappers;

    @Before
    public void before() {
        wrappers = Arrays.asList(new ValueWrapper(1), new ValueWrapper(2), new ValueWrapper(2), null);
    }

    @Test
    public void test1() {
        Map<Integer, List<ValueWrapper>> target = new HashMap<>();
        target.put(1, Collections.singletonList(new ValueWrapper(1)));
        target.put(2, Arrays.asList(new ValueWrapper(2), new ValueWrapper(2)));

        Assert.assertEquals(target, CollectionUtil.group(wrappers, ValueWrapper::getValue));
    }

    @Test
    public void empty() {
        Assert.assertEquals(Collections.emptyMap(), CollectionUtil.group(null, ValueWrapper::getValue));
    }
}
