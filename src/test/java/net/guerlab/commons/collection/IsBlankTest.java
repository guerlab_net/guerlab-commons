package net.guerlab.commons.collection;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author guer
 */
public class IsBlankTest extends TestBefore {

    @Test
    public void collTest1() {
        Assert.assertFalse(CollectionUtil.isBlank(coll1));
    }

    @Test
    public void collTest2() {
        Assert.assertTrue(CollectionUtil.isBlank(coll2));
    }

    @Test
    public void collTest3() {
        Assert.assertTrue(CollectionUtil.isBlank(coll3));
    }
}
