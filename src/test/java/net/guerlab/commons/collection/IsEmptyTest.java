package net.guerlab.commons.collection;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author guer
 */
public class IsEmptyTest extends TestBefore {

    @Test
    public void collTest1() {
        Assert.assertFalse(CollectionUtil.isEmpty(coll1));
    }

    @Test
    public void collTest2() {
        Assert.assertFalse(CollectionUtil.isEmpty(coll2));
    }

    @Test
    public void collTest3() {
        Assert.assertTrue(CollectionUtil.isEmpty(coll3));
    }

    @Test
    public void iterableTest1() {
        Assert.assertFalse(CollectionUtil.isEmpty(iterable1));
    }

    @Test
    public void iterableTest2() {
        Assert.assertFalse(CollectionUtil.isEmpty(iterable2));
    }

    @Test
    public void iterableTest3() {
        Assert.assertTrue(CollectionUtil.isEmpty(iterable3));
    }

    @Test
    public void iteratorTest1() {
        Assert.assertFalse(CollectionUtil.isEmpty(iterator1));
    }

    @Test
    public void iteratorTest2() {
        Assert.assertFalse(CollectionUtil.isEmpty(iterator2));
    }

    @Test
    public void iteratorTest3() {
        Assert.assertTrue(CollectionUtil.isEmpty(iterator3));
    }

    @Test
    public void mapTest1() {
        Assert.assertFalse(CollectionUtil.isEmpty(map1));
    }

    @Test
    public void mapTest2() {
        Assert.assertFalse(CollectionUtil.isEmpty(map2));
    }

    @Test
    public void mapTest3() {
        Assert.assertFalse(CollectionUtil.isEmpty(map3));
    }

    @Test
    public void mapTest4() {
        Assert.assertFalse(CollectionUtil.isEmpty(map4));
    }

    @Test
    public void mapTest5() {
        Assert.assertTrue(CollectionUtil.isEmpty(map5));
    }

    @Test
    public void enumerationTest1() {
        Assert.assertFalse(CollectionUtil.isEmpty(enumeration1));
    }

    @Test
    public void enumerationTest2() {
        Assert.assertTrue(CollectionUtil.isEmpty(enumeration2));
    }
}
