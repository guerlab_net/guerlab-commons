package net.guerlab.commons.collection;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author guer
 */
public class IsNotBlankTest extends TestBefore {

    @Test
    public void collTest1() {
        Assert.assertTrue(CollectionUtil.isNotBlank(coll1));
    }

    @Test
    public void collTest2() {
        Assert.assertFalse(CollectionUtil.isNotBlank(coll2));
    }

    @Test
    public void collTest3() {
        Assert.assertFalse(CollectionUtil.isNotBlank(coll3));
    }
}
