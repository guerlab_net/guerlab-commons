package net.guerlab.commons.collection;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author guer
 */
public class IsNotEmptyTest extends TestBefore {

    @Test
    public void collTest1() {
        Assert.assertTrue(CollectionUtil.isNotEmpty(coll1));
    }

    @Test
    public void collTest2() {
        Assert.assertTrue(CollectionUtil.isNotEmpty(coll2));
    }

    @Test
    public void collTest3() {
        Assert.assertFalse(CollectionUtil.isNotEmpty(coll3));
    }

    @Test
    public void iterableTest1() {
        Assert.assertTrue(CollectionUtil.isNotEmpty(iterable1));
    }

    @Test
    public void iterableTest2() {
        Assert.assertTrue(CollectionUtil.isNotEmpty(iterable2));
    }

    @Test
    public void iterableTest3() {
        Assert.assertFalse(CollectionUtil.isNotEmpty(iterable3));
    }

    @Test
    public void iteratorTest1() {
        Assert.assertTrue(CollectionUtil.isNotEmpty(iterator1));
    }

    @Test
    public void iteratorTest2() {
        Assert.assertTrue(CollectionUtil.isNotEmpty(iterator2));
    }

    @Test
    public void iteratorTest3() {
        Assert.assertFalse(CollectionUtil.isNotEmpty(iterator3));
    }

    @Test
    public void mapTest1() {
        Assert.assertTrue(CollectionUtil.isNotEmpty(map1));
    }

    @Test
    public void mapTest2() {
        Assert.assertTrue(CollectionUtil.isNotEmpty(map2));
    }

    @Test
    public void mapTest3() {
        Assert.assertTrue(CollectionUtil.isNotEmpty(map3));
    }

    @Test
    public void mapTest4() {
        Assert.assertTrue(CollectionUtil.isNotEmpty(map4));
    }

    @Test
    public void mapTest5() {
        Assert.assertFalse(CollectionUtil.isNotEmpty(map5));
    }

    @Test
    public void enumerationTest1() {
        Assert.assertTrue(CollectionUtil.isNotEmpty(enumeration1));
    }

    @Test
    public void enumerationTest2() {
        Assert.assertFalse(CollectionUtil.isNotEmpty(enumeration2));
    }
}
