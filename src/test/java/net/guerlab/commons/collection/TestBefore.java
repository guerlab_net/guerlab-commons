package net.guerlab.commons.collection;

import org.junit.Before;

import java.util.*;

/**
 * @author guer
 */
public class TestBefore {

    protected Collection<Integer> coll1;

    protected Collection<Integer> coll2;

    protected Collection<Integer> coll3;

    protected Iterable<Integer> iterable1;

    protected Iterable<Integer> iterable2;

    protected Iterable<Integer> iterable3;

    protected Iterator<Integer> iterator1;

    protected Iterator<Integer> iterator2;

    protected Iterator<Integer> iterator3;

    protected Map<Integer, Integer> map1;

    protected Map<Integer, Integer> map2;

    protected Map<Integer, Integer> map3;

    protected Map<Integer, Integer> map4;

    protected Map<Integer, Integer> map5;

    protected Enumeration<Object> enumeration1;

    protected Enumeration<Object> enumeration2;

    @SuppressWarnings({ "unchecked", "RedundantOperationOnEmptyContainer" })
    @Before
    public void before() {
        coll1 = Arrays.asList(1, 2, null);
        coll2 = Collections.singletonList(null);
        coll3 = Collections.emptyList();

        iterable1 = Arrays.asList(1, 2, null);
        iterable2 = Collections.singletonList(null);
        iterable3 = Collections.emptyList();

        iterator1 = Arrays.asList(1, 2, null).iterator();
        iterator2 = Collections.singletonList((Integer) null).iterator();
        iterator3 = ((List<Integer>) Collections.EMPTY_LIST).iterator();

        map1 = Collections.singletonMap(1, 1);
        map2 = Collections.singletonMap(1, null);
        map3 = Collections.singletonMap(null, 1);
        map4 = Collections.singletonMap(null, null);
        map5 = Collections.emptyMap();

        enumeration1 = new StringTokenizer("value");
        enumeration2 = new StringTokenizer("");
    }
}
