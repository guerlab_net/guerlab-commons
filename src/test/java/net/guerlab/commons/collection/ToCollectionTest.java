package net.guerlab.commons.collection;

import net.guerlab.commons.ValueWrapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/**
 * @author guer
 */
public class ToCollectionTest {

    private Collection<ValueWrapper> wrappers;

    @Before
    public void before() {
        wrappers = Arrays.asList(new ValueWrapper(1), new ValueWrapper(2), null);
    }

    @Test
    public void test1() {
        Assert.assertEquals(Arrays.asList(1, 2),
                CollectionUtil.toCollection(wrappers, ValueWrapper::getValue, ArrayList::new, Collections.emptyList()));
    }

    @Test
    public void test2() {
        Assert.assertEquals(Collections.emptyList(),
                CollectionUtil.toCollection(null, ValueWrapper::getValue, ArrayList::new, Collections.emptyList()));
    }
}
