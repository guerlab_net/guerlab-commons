package net.guerlab.commons.collection;

import net.guerlab.commons.ValueWrapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;

/**
 * @author guer
 */
public class ToDistinctListTest {

    private Collection<ValueWrapper> wrappers;

    @Before
    public void before() {
        wrappers = Arrays.asList(new ValueWrapper(1), new ValueWrapper(2), new ValueWrapper(2), null);
    }

    @Test
    public void test1() {
        Assert.assertEquals(Arrays.asList(1, 2), CollectionUtil.toDistinctList(wrappers, ValueWrapper::getValue));
    }
}
