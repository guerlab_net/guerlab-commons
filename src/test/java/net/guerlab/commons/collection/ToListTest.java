package net.guerlab.commons.collection;

import net.guerlab.commons.ValueWrapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;

/**
 * @author guer
 */
public class ToListTest {

    private Collection<ValueWrapper> wrappers;

    @Before
    public void before() {
        wrappers = Arrays.asList(new ValueWrapper(1), new ValueWrapper(2), null);
    }

    @Test
    public void test1() {
        Assert.assertEquals(CollectionUtil.toList(wrappers, ValueWrapper::getValue), Arrays.asList(1, 2));
    }
}
