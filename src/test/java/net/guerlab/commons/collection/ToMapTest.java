package net.guerlab.commons.collection;

import net.guerlab.commons.ValueWrapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.*;
import java.util.function.Function;

/**
 * @author guer
 */
public class ToMapTest {

    private Collection<ValueWrapper> wrappers;

    @Before
    public void before() {
        wrappers = Arrays.asList(new ValueWrapper(1), new ValueWrapper(2), null);
    }

    @Test
    public void test1() {
        Map<Integer, ValueWrapper> target = new HashMap<>();
        target.put(1, new ValueWrapper(1));
        target.put(2, new ValueWrapper(2));

        Assert.assertEquals(CollectionUtil.toMap(wrappers, ValueWrapper::getValue), target);
    }

    @Test
    public void test2() {
        Map<Integer, ValueWrapper> target = new HashMap<>();
        target.put(2, new ValueWrapper(2));

        Assert.assertEquals(CollectionUtil
                        .toMap(wrappers, ValueWrapper::getValue, Collections.singletonList(wrapper -> wrapper.value % 2 == 0)),
                target);
    }

    @Test
    public void test3() {
        Assert.assertEquals(CollectionUtil.toMap(Collections.emptyList(), ValueWrapper::getValue),
                Collections.emptyMap());
    }

    @Test(expected = IllegalStateException.class)
    public void test4() {
        wrappers = Arrays.asList(new ValueWrapper(1), new ValueWrapper(2), null, new ValueWrapper(2));
        CollectionUtil.toMap(wrappers, ValueWrapper::getValue);
    }

    @Test
    public void test5() {
        CollectionUtil.toMap(wrappers, ValueWrapper::getValue, Function.identity(), null, HashMap::new,
                Collections.emptyList());
    }
}
