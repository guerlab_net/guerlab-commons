package net.guerlab.commons.encrypt;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author guer
 */
public class EncryptTest {

    protected String key;

    protected long expiry;

    protected int randomKeyLength;

    @Before
    public void before() {
        key = "key";
        expiry = 1800000L;
        randomKeyLength = 5;
    }

    @Test
    public void test1() {
        String data = "date content";
        String encode = AuthCodeHelper.encode(data, key, expiry);
        Assert.assertEquals(data, AuthCodeHelper.decode(encode, key));
    }

    @Test(expected = NullPointerException.class)
    public void test2() {
        AuthCodeHelper.encode(null, key, expiry);
    }

    @Test
    public void test3() {
        String data = "date content";
        String encode = AuthCodeHelper.encode(data, key, expiry, randomKeyLength);
        Assert.assertEquals(data, AuthCodeHelper.decode(encode, key, randomKeyLength));
    }

    @Test(expected = NullPointerException.class)
    public void test4() {
        AuthCodeHelper.decode(null, key);
    }
}
