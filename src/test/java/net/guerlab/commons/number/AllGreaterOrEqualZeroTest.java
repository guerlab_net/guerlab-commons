package net.guerlab.commons.number;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author guer
 */
public class AllGreaterOrEqualZeroTest {

    @Test
    public void test1() {
        Assert.assertFalse(NumberHelper.allGreaterOrEqualZero(-1, 0, 1));
    }

    @Test
    public void test2() {
        Assert.assertFalse(NumberHelper.allGreaterOrEqualZero(-1, 0));
    }

    @Test
    public void test3() {
        Assert.assertTrue(NumberHelper.allGreaterOrEqualZero(0, 1));
    }

    @Test
    public void test4() {
        Assert.assertFalse(NumberHelper.allGreaterOrEqualZero(-1, 1));
    }
}
