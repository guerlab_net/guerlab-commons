package net.guerlab.commons.number;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author guer
 */
public class AllLessZeroTest {

    @Test
    public void test1() {
        Assert.assertFalse(NumberHelper.allLessZero(-1, 0, 1));
    }

    @Test
    public void test2() {
        Assert.assertFalse(NumberHelper.allLessZero(-1, 0));
    }

    @Test
    public void test3() {
        Assert.assertFalse(NumberHelper.allLessZero(0, 1));
    }

    @Test
    public void test4() {
        Assert.assertFalse(NumberHelper.allLessZero(-1, 1));
    }
}
