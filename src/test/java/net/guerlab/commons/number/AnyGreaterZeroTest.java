package net.guerlab.commons.number;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author guer
 */
public class AnyGreaterZeroTest {

    @Test
    public void test1() {
        Assert.assertTrue(NumberHelper.anyGreaterZero(-1, 0, 1));
    }

    @Test
    public void test2() {
        Assert.assertFalse(NumberHelper.anyGreaterZero(-1, 0));
    }

    @Test
    public void test3() {
        Assert.assertTrue(NumberHelper.anyGreaterZero(0, 1));
    }

    @Test
    public void test4() {
        Assert.assertTrue(NumberHelper.anyGreaterZero(-1, 1));
    }
}
