package net.guerlab.commons.number;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author guer
 */
public class AnyLessZeroTest {

    @Test
    public void test1() {
        Assert.assertTrue(NumberHelper.anyLessZero(-1, 0, 1));
    }

    @Test
    public void test2() {
        Assert.assertTrue(NumberHelper.anyLessZero(-1, 0));
    }

    @Test
    public void test3() {
        Assert.assertFalse(NumberHelper.anyLessZero(0, 1));
    }

    @Test
    public void test4() {
        Assert.assertTrue(NumberHelper.anyLessZero(-1, 1));
    }
}
