package net.guerlab.commons.number;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author guer
 */
public class GreaterOrEqualZeroTest {

    @Test
    public void test1() {
        Assert.assertTrue(NumberHelper.greaterOrEqualZero(1));
    }

    @Test
    public void test2() {
        Assert.assertTrue(NumberHelper.greaterOrEqualZero(0));
    }

    @Test
    public void test3() {
        Assert.assertFalse(NumberHelper.greaterOrEqualZero(-1));
    }
}
