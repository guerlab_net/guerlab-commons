package net.guerlab.commons.number;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author guer
 */
public class GreaterZeroTest {

    @Test
    public void test1() {
        Assert.assertTrue(NumberHelper.greaterZero(1));
    }

    @Test
    public void test2() {
        Assert.assertFalse(NumberHelper.greaterZero(0));
    }

    @Test
    public void test3() {
        Assert.assertFalse(NumberHelper.greaterZero(-1));
    }
}
