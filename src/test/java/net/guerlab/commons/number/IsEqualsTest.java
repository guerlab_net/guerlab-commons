package net.guerlab.commons.number;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author guer
 */
public class IsEqualsTest {

    @Test
    public void test1() {
        Assert.assertTrue(NumberHelper.isEquals(1, 1));
    }

    @Test
    public void test2() {
        Assert.assertFalse(NumberHelper.isEquals(1, -1));
    }
}
