package net.guerlab.commons.number;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author guer
 */
public class LessOrEqualZeroTest {

    @Test
    public void test1() {
        Assert.assertFalse(NumberHelper.lessOrEqualZero(1));
    }

    @Test
    public void test2() {
        Assert.assertTrue(NumberHelper.lessOrEqualZero(0));
    }

    @Test
    public void test3() {
        Assert.assertTrue(NumberHelper.lessOrEqualZero(-1));
    }
}
