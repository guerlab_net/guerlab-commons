package net.guerlab.commons.number;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author guer
 */
public class LessZeroTest {

    @Test
    public void test1() {
        Assert.assertFalse(NumberHelper.lessZero(1));
    }

    @Test
    public void test2() {
        Assert.assertFalse(NumberHelper.lessZero(0));
    }

    @Test
    public void test3() {
        Assert.assertTrue(NumberHelper.lessZero(-1));
    }
}
