package net.guerlab.commons.random;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author guer
 */
public class RandomUtilTest {

    @Test
    public void stringTest1() {
        Assert.assertEquals(RandomUtil.nextString(-1), "");
    }

    @Test
    public void stringTest2() {
        Assert.assertEquals(RandomUtil.nextString(1, new char[] { 'A' }), "A");
    }

    @Test
    public void stringTest3() {
        Assert.assertNotNull(RandomUtil.nextString(5, new char[] {}));
    }

    @Test
    public void intTest1() {
        Assert.assertTrue(RandomUtil.nextInt(5) < 5);
    }

    @Test
    public void intTest2() {
        int value = RandomUtil.nextInt(5, 10);
        Assert.assertTrue(value >= 5 && value < 10);
    }

    @Test
    public void intTest3() {
        Assert.assertNotEquals(RandomUtil.nextInt(), RandomUtil.nextInt());
    }

    @Test
    public void longTest1() {
        Assert.assertTrue(RandomUtil.nextLong(5L) < 5L);
    }

    @Test
    public void longTest2() {
        long value = RandomUtil.nextLong(5L, 10L);
        Assert.assertTrue(value >= 5L && value < 10L);
    }

    @Test
    public void floatTest() {
        float value = RandomUtil.nextFloat();
        Assert.assertTrue(value > 0 && value < 1);
    }

    @Test
    public void doubleTest1() {
        double value = RandomUtil.nextDouble(5.0);
        Assert.assertTrue(value < 5.0);
    }

    @Test
    public void doubleTest2() {
        double value = RandomUtil.nextDouble(5.0, 10.0);
        Assert.assertTrue(value >= 5.0 && value < 10.0);
    }

    @Test
    public void bigDecimalTest() {
        Assert.assertNotNull(RandomUtil.nextBigDecimal());
    }

    @Test
    public void bigIntegerTest() {
        Assert.assertNotNull(RandomUtil.nextBigInteger());
    }
}
