package net.guerlab.commons.reflection;

import net.guerlab.commons.Boo;
import net.guerlab.commons.EmptyObject;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.Objects;

/**
 * 读取/设置测试
 *
 * @author guer
 */
public class GetFieldsTest {

    @Test
    public void getPublicValue1() {
        Assert.assertFalse(
                FieldUtil.getFieldsWithFilter(Boo.class, field -> Objects.equals(field.getName(), "publicValue"))
                        .isEmpty());
    }

    @Test
    public void emptyObject() {
        Assert.assertTrue(FieldUtil.getFields(EmptyObject.class).isEmpty());
    }

    @Test
    public void nullPoint() {
        Assert.assertTrue(FieldUtil.getFields(null).isEmpty());
    }

    @Test
    public void nullPoint2() {
        Assert.assertTrue(FieldUtil.getFieldsWithFilter(null, Collections.emptyList()).isEmpty());
    }

}
