package net.guerlab.commons.reflection;

import net.guerlab.commons.Foo;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Field;

/**
 * 读取/设置测试
 *
 * @author guer
 */
public class GetTest extends InitObject {

    @Test
    public void publicValue() {
        Assert.assertNotNull(FieldUtil.get(boo, "publicValue"));
    }

    @Test
    public void privateValue() {
        Assert.assertNotNull(FieldUtil.get(boo, "privateValue"));
    }

    @Test
    public void unknownValue() {
        Assert.assertNull(FieldUtil.get(boo, "unknownValue"));
    }

    @Test
    public void otherObjectField() {
        Field field = FieldUtil.getField(Foo.class, "stringValue");
        Assert.assertNull(FieldUtil.get(boo, field));
    }
}
