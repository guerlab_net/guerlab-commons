package net.guerlab.commons.reflection;

import net.guerlab.commons.Boo;
import org.junit.Before;

/**
 * 初始化对象
 *
 * @author guer
 */
public class InitObject {

    protected Boo boo;

    @Before
    public void beforeTest() {
        boo = new Boo();
        boo.publicValue = "publicValue";
        boo.setPrivateValue("privateValue");
    }
}
