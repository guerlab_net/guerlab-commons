package net.guerlab.commons.reflection;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * 读取/设置测试
 *
 * @author guer
 */
public class NumberTest {

    @Test
    public void integerType() {
        Assert.assertTrue(FieldUtil.isNumberClass(Integer.TYPE));
    }

    @Test
    public void bigDecimal() {
        Assert.assertTrue(FieldUtil.isNumberClass(BigDecimal.class));
    }

    @Test
    public void string() {
        Assert.assertFalse(FieldUtil.isNumberClass(String.class));
    }

    @Test
    public void nullPoint() {
        Assert.assertFalse(FieldUtil.isNumberClass(null));
    }
}
