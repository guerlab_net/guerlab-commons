package net.guerlab.commons.reflection;

import net.guerlab.commons.Boo;
import net.guerlab.commons.Foo;
import org.junit.Assert;
import org.junit.Test;

/**
 * 读取/设置测试
 *
 * @author guer
 */
public class PutTest extends InitObject {

    @Test
    public void publicValue() {
        Boo target = new Boo();
        FieldUtil.put(target, "publicValue", FieldUtil.get(boo, "publicValue"));
        Assert.assertEquals(target.publicValue, boo.publicValue);
    }

    @Test
    public void privateValue() {
        Boo target = new Boo();
        FieldUtil.put(target, "privateValue", FieldUtil.get(boo, "publicValue"));
        Assert.assertEquals(target.getPrivateValue(), boo.publicValue);
    }

    @Test
    public void stringValue() {
        Foo foo = new Foo();
        FieldUtil.put(foo, "stringValue", FieldUtil.get(boo, "publicValue"));
        Assert.assertEquals(foo.getStringValue(), boo.publicValue);
    }

    @Test
    public void intValue() {
        Foo foo = new Foo();
        FieldUtil.put(foo, "intValue", FieldUtil.get(boo, "publicValue"));
        Assert.assertNull(foo.getIntValue());
    }

    @Test
    public void unknownValue() {
        Foo foo = new Foo();
        FieldUtil.put(foo, "intValue", FieldUtil.get(boo, "publicValue"));
        Assert.assertNull(foo.getIntValue());
    }
}
