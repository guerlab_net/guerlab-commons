package net.guerlab.commons.reflection;

import org.junit.Assert;
import org.junit.Test;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;

/**
 * 读取/设置测试
 *
 * @author guer
 */
public class ReadTest extends InitObject {

    @Test
    public void publicValue() throws IntrospectionException, InvocationTargetException, IllegalAccessException {
        Assert.assertNotNull(FieldUtil.read(boo, "publicValue"));
    }

    @Test
    public void privateValue() throws IntrospectionException, InvocationTargetException, IllegalAccessException {
        Assert.assertNotNull(FieldUtil.read(boo, "privateValue"));
    }

    @Test
    public void unknownValue() throws IntrospectionException, InvocationTargetException, IllegalAccessException {
        Assert.assertNull(FieldUtil.read(boo, "unknownValue"));
    }
}
