package net.guerlab.commons.reflection;

import net.guerlab.commons.Boo;
import net.guerlab.commons.Foo;
import net.guerlab.commons.SubFoo;
import org.junit.Test;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;

/**
 * 读取/设置测试
 *
 * @author guer
 */
public class WriteTest extends InitObject {

    @Test
    public void publicValue() throws IntrospectionException, InvocationTargetException, IllegalAccessException {
        FieldUtil.write(new Boo(), "publicValue", FieldUtil.get(boo, "publicValue"));
    }

    @Test
    public void privateValue() throws IntrospectionException, InvocationTargetException, IllegalAccessException {
        FieldUtil.write(new Boo(), "privateValue", FieldUtil.get(boo, "privateValue"));
    }

    @Test
    public void unknownValue() throws IntrospectionException, InvocationTargetException, IllegalAccessException {
        FieldUtil.write(new Foo(), "unknownValue", FieldUtil.get(boo, "publicValue"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwException() throws IntrospectionException, InvocationTargetException, IllegalAccessException {
        FieldUtil.write(new SubFoo(), "intValue", FieldUtil.get(boo, "publicValue"));
    }
}
