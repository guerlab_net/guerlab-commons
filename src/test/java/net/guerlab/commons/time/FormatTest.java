package net.guerlab.commons.time;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.temporal.TemporalAccessor;
import java.util.Date;

/**
 * 格式化测试
 *
 * @author guer
 */
public class FormatTest {

    private ZoneId zoneId;

    private ZoneOffset zoneOffset;

    private LocalDateTime time;

    private Date date;

    @Before
    public void before() {
        zoneId = ZoneId.systemDefault();
        zoneOffset = TimeHelper.getZoneOffset(zoneId);
        time = LocalDateTime.of(2021, 6, 1, 0, 0, 0);
        date = Date.from(time.toInstant(zoneOffset));
    }

    @Test
    public void timeTest0() {
        Assert.assertEquals(TimeHelper.format(time, "yyyy-MM-dd HH:mm:ss"), "2021-06-01 00:00:00");
    }

    @Test
    public void timeWithNullTest0() {
        Assert.assertNull(TimeHelper.format(time, ""));
    }

    @Test
    public void timeTest1() {
        Assert.assertEquals(TimeHelper.format(time), "2021-06-01 00:00:00");
    }

    @Test
    public void timeWithNullTest1() {
        Assert.assertNull(TimeHelper.format((TemporalAccessor) null));
    }

    @Test
    public void timeTest2() {
        Assert.assertEquals(TimeHelper.formatDate(time), "2021-06-01");
    }

    @Test
    public void timeWithNullTest2() {
        Assert.assertNull(TimeHelper.formatDate((TemporalAccessor) null));
    }

    @Test
    public void dateTest0() {
        Assert.assertEquals(TimeHelper.format(date, "yyyy-MM-dd HH:mm:ss"), "2021-06-01 00:00:00");
    }

    @Test
    public void dateWithNullTest0() {
        Assert.assertNull(TimeHelper.format(date, ""));
    }

    @Test
    public void dateTest1() {
        Assert.assertEquals(TimeHelper.format(date), "2021-06-01 00:00:00" + zoneOffset + "[" + zoneId + "]");
    }

    @Test
    public void dateWithNullTest1() {
        Assert.assertNull(TimeHelper.format((Date) null));
    }

    @Test
    public void dateTest2() {
        Assert.assertEquals(TimeHelper.formatDate(date), "2021-06-01" + zoneOffset);
    }

    @Test
    public void dateWithNullTest2() {
        Assert.assertNull(TimeHelper.formatDate((Date) null));
    }
}
