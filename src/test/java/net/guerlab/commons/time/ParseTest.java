package net.guerlab.commons.time;

import org.junit.Assert;
import org.junit.Test;

/**
 * 解析测试
 *
 * @author guer
 */
public class ParseTest {

	//@formatter:off
    final Value[] values = new Value[] {
            new Value(System.currentTimeMillis() + "", true, true, true, true),
            new Value("2020-12-21T17:07:21.123", true, true, true, true),
            new Value("2020-12-21T17:07:21", true, true, true, true),
            new Value("2020-12-21T17:07", true, true, true, true),
            new Value("2020-12-21T17:07:21.123Z", true, true, true, true),
            new Value("2020-12-21T17:07:21Z", true, true, true, true),
            new Value("2020-12-21T17:07Z", true, true, true, true),
            new Value("2020-12-21 17:07:21.123", true, true, true, true),
            new Value("2020-12-21 17:07:21", true, true, true, true),
            new Value("2020-12-21 17:07", true, true, true, true),
            new Value("2020-12-21", true, true, false, true),
            new Value("17:07:21", false, false, true, false),
            new Value("17:07", false, false, true, false),
            new Value(" ", false, false, false, false),
            new Value("", false, false, false, false),
            new Value(null, false, false, false, false),
            new Value("123456-1234", false, false, false, false),
    };
    //@formatter:on

	@Test
	public void test0() {
		test(values[0]);
	}

	@Test
	public void test1() {
		test(values[1]);
	}

	@Test
	public void test2() {
		test(values[2]);
	}

	@Test
	public void test3() {
		test(values[3]);
	}

	@Test
	public void test4() {
		test(values[4]);
	}

	@Test
	public void test5() {
		test(values[5]);
	}

	@Test
	public void test6() {
		test(values[6]);
	}

	@Test
	public void test7() {
		test(values[7]);
	}

	@Test
	public void test8() {
		test(values[8]);
	}

	@Test
	public void test9() {
		test(values[9]);
	}

	@Test
	public void test10() {
		test(values[10]);
	}

	@Test
	public void test11() {
		test(values[11]);
	}

	@Test
	public void test12() {
		test(values[12]);
	}

	@Test
	public void test13() {
		test(values[13]);
	}

	@Test
	public void test14() {
		test(values[14]);
	}

	@Test
	public void test15() {
		test(values[15]);
	}

	@Test
	public void test16() {
		test(values[16]);
	}

	public void test(Value value) {
		if (value.hasDateTime) {
			Assert.assertNotNull(TimeHelper.parseLocalDateTime(value.value));
		}
		else {
			Assert.assertNull(TimeHelper.parseLocalDateTime(value.value));
		}

		if (value.hasDate) {
			Assert.assertNotNull(TimeHelper.parseLocalDate(value.value));
		}
		else {
			Assert.assertNull(TimeHelper.parseLocalDate(value.value));
		}

		if (value.hasTime) {
			Assert.assertNotNull(TimeHelper.parseLocalTime(value.value));
		}
		else {
			Assert.assertNull(TimeHelper.parseLocalTime(value.value));
		}

		if (value.hasParse) {
			Assert.assertNotNull(TimeHelper.parse(value.value));
			Assert.assertNotNull(TimeHelper.parse(value.value, TimeHelper.INSTANT_TEMPORAL_QUERY));
		}
		else {
			Assert.assertNull(TimeHelper.parse(value.value));
			Assert.assertNull(TimeHelper.parse(value.value, TimeHelper.INSTANT_TEMPORAL_QUERY));
		}
	}

	static class Value {

		final String value;

		final boolean hasDateTime;

		final boolean hasDate;

		final boolean hasTime;

		final boolean hasParse;

		public Value(String value, boolean hasDateTime, boolean hasDate, boolean hasTime, boolean hasParse) {
			this.value = value;
			this.hasDateTime = hasDateTime;
			this.hasDate = hasDate;
			this.hasTime = hasTime;
			this.hasParse = hasParse;
		}
	}
}
